package com.diogosoares.climatempo.repositorios;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.diogosoares.climatempo.models.Contato;
import com.diogosoares.climatempo.utils.BdHelper;

import java.util.ArrayList;

public class RepositorioContatos {
    static private RepositorioContatos singleton;
    private SQLiteDatabase bDContatos;
    public static RepositorioContatos getSingleton(Context context){
        if (singleton == null) {
            singleton = new RepositorioContatos();
            singleton.bDContatos = new BdHelper(context,"",null,0).getWritableDatabase();
        }

        return singleton;
    }

    private ArrayList<Contato> listaContato = new ArrayList<>();

    public ArrayList<Contato> getListaContato() {
        String[] projection = {
                "id",
                "nome",
                "email",
                "telefone",
                "endereco"
        };
        Cursor cursor = bDContatos.query("contatos", projection,null,
                null,null,null,null);
        ArrayList itens = new ArrayList<Contato>();
        while(cursor.moveToNext()) {
            long id = cursor.getLong(cursor.getColumnIndexOrThrow("id"));
            String nome = cursor.getString(
                    cursor.getColumnIndexOrThrow("nome"));
            String email = cursor.getString(
                    cursor.getColumnIndexOrThrow("email"));
            String telefone = cursor.getString(
                    cursor.getColumnIndexOrThrow("telefone"));
            String endereco = cursor.getString(
                    cursor.getColumnIndexOrThrow("endereco"));
            Contato contato = new Contato(id,nome,telefone,email,endereco);
            itens.add(contato);
        }
        cursor.close();
        return itens;
    }
    public void addContato(Contato contato){
        listaContato.add(contato);
        ContentValues values = new ContentValues();
        values.put("nome", contato.getNome());
        values.put("email", contato.getEmail());
        values.put("telefone", contato.getNumero());
        values.put("endereco", contato.getEndereco());
        bDContatos.insertOrThrow("contatos", null, values);
    }

    public void removeContato(Contato contato) {
        listaContato.remove(contato);
        String selection = " id = ?";
        String[] selectionArgs = { String.valueOf(contato.getId()) };

        bDContatos.delete("contatos", selection, selectionArgs);
    }

    public void updateContato(Contato contato) {
        ContentValues values = new ContentValues();
        values.put("nome", contato.getNome());
        values.put("email", contato.getEmail());
        values.put("telefone", contato.getNumero());
        values.put("endereco", contato.getEndereco());

        String selection = " id = ?";
        String[] selectionArgs = { String.valueOf(contato.getId()) };

        int count = bDContatos.update(
                "contatos",
                values,
                selection,
                selectionArgs);
    }

}
