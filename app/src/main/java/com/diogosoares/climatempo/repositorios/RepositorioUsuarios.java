package com.diogosoares.climatempo.repositorios;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.diogosoares.climatempo.models.Contato;
import com.diogosoares.climatempo.models.Usuario;
import com.diogosoares.climatempo.utils.BdHelper;

import java.util.ArrayList;

public class RepositorioUsuarios {
    SQLiteDatabase dbUsuarios;
    static RepositorioUsuarios singleton;

    public static RepositorioUsuarios getDbUsuarios(Context context) {

        if (singleton == null) {
            singleton = new RepositorioUsuarios();
            singleton.dbUsuarios = new BdHelper(context,"",null,0).getWritableDatabase();
        }
        return singleton;
    }
    public void addUsuario(Usuario usuario){
        ContentValues values = new ContentValues();
        values.put("username", usuario.getUsername());
        values.put("senha", usuario.getSenha());

        dbUsuarios.insertOrThrow("usuarios", null, values);

    }
    public ArrayList<Usuario> getUsuarios(){
        String[] projection = {
                "id",
                "username",
                "senha"
        };
        Cursor cursor = dbUsuarios.query("usuarios", projection,null,
                null,null,null,null);
        ArrayList itens = new ArrayList<Usuario>();
        while(cursor.moveToNext()) {
            long id = cursor.getLong(cursor.getColumnIndexOrThrow("id"));
            String username = cursor.getString(
                    cursor.getColumnIndexOrThrow("username"));
            String senha = cursor.getString(
                    cursor.getColumnIndexOrThrow("senha"));

            Usuario usuario = new Usuario(id,username,senha);
            itens.add(usuario);
        }
        cursor.close();
        return itens;
    }
}
