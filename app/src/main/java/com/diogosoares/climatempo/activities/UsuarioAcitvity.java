package com.diogosoares.climatempo.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.diogosoares.climatempo.R;
import com.diogosoares.climatempo.models.Usuario;
import com.diogosoares.climatempo.repositorios.RepositorioUsuarios;

public class UsuarioAcitvity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_usuario_acitvity);
    }

    public void logarUsuario(View view) {
        EditText username = findViewById(R.id.editEmail);
        EditText senha = findViewById(R.id.editSenha);


        Usuario usuario = new Usuario(0l, username.getText().toString(), senha.getText().toString());
        RepositorioUsuarios reposit = RepositorioUsuarios.getDbUsuarios(this);

        reposit.addUsuario(usuario);
    }
}
