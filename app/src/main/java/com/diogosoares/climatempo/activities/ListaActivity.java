package com.diogosoares.climatempo.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.diogosoares.climatempo.utils.Adapter;
import com.diogosoares.climatempo.models.Contato;
import com.diogosoares.climatempo.R;
import com.diogosoares.climatempo.repositorios.RepositorioContatos;

public class ListaActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, Adapter.onContactItemListener {

    private ListView lista;
    private Adapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista);
        lista = findViewById(R.id.lista);

        lista.setOnItemClickListener(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        RepositorioContatos repositorio = RepositorioContatos.getSingleton(this);
        adapter = new Adapter(this, 0, repositorio.getListaContato(), this);
        lista.setAdapter(adapter);
    }

    public void criarNovoContato(View view) {
        Intent intent = new Intent(this, FormularioActivity.class);
        startActivity(intent);
    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int position, long l) {
        Intent intent = new Intent(this, FormularioActivity.class);
        intent.putExtra("Contato", adapter.getItem(position));
        startActivity(intent);
    }

    @Override
    public void onTrashClick(Contato contatoToRemove) {
        adapter.remove(contatoToRemove);
        RepositorioContatos.getSingleton(this).removeContato(contatoToRemove);
        adapter.notifyDataSetChanged();
    }
}
