package com.diogosoares.climatempo.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.diogosoares.climatempo.R;
import com.diogosoares.climatempo.activities.ListaActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }

    public void irParaOutraPagina(View view) {
        Intent vaiParaLista = new Intent(this, ListaActivity.class);
        startActivity(vaiParaLista);
    }

    public void irParaCadastro(View view) {
        Intent intent = new Intent(this, UsuarioAcitvity.class);
        startActivity(intent);
    }
}
