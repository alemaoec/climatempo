package com.diogosoares.climatempo.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import com.diogosoares.climatempo.models.Contato;
import com.diogosoares.climatempo.R;
import com.diogosoares.climatempo.repositorios.RepositorioContatos;

public class FormularioActivity extends AppCompatActivity {

    private EditText nome;
    private EditText telefone;
    private EditText email;
    private EditText endereco;
    private Contato contato;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario);
        Intent intent = getIntent();
        bind();
        if (intent.hasExtra("Contato")) {
            contato = (Contato) intent.getExtras().getSerializable("Contato");
            setContato(contato);
        }
    }

    public void salvarContato(View view) {

        Contato contato = new Contato(0l, nome.getText().toString(), telefone.getText().toString(), email.getText().toString(), endereco.getText().toString());
        RepositorioContatos repositorio = RepositorioContatos.getSingleton(this);

        if (this.contato != null) {
            contato.setId(this.contato.getId());
            repositorio.updateContato(contato);
        } else {
            repositorio.addContato(contato);
        }

        finish();
    }

    public void bind() {
        nome = (EditText) findViewById(R.id.nomeEdit);
        telefone = (EditText) findViewById(R.id.telefoneEdit);
        email = (EditText) findViewById(R.id.emailEdit);
        endereco = (EditText) findViewById(R.id.enderecoEdit);
    }

    public void setContato(Contato contato) {
        nome.setText(contato.getNome());
        email.setText(contato.getEmail());
        telefone.setText(contato.getNumero());
        endereco.setText(contato.getEndereco());
    }
}
