package com.diogosoares.climatempo.utils;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.diogosoares.climatempo.models.Contato;
import com.diogosoares.climatempo.R;

import java.util.List;


public class Adapter extends ArrayAdapter<Contato> {

    private final onContactItemListener callback;

    public interface  onContactItemListener{
        public void onTrashClick(Contato contatoToRemove);
    }

    public Adapter(@NonNull Context context, int resource, @NonNull List<Contato> objects, onContactItemListener callback) {
        super(context, resource, objects);
        this.callback = callback;

    }

    @NonNull
    @Override
    public View getView(final int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflator = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewItem = inflator.inflate(R.layout.item_contato, null);
        Contato contato = getItem(position);
        TextView nomeText = viewItem.findViewById(R.id.itemNome);
        nomeText.setText(contato.getNome());
        TextView telefoneText = viewItem.findViewById(R.id.itemTelefone);
        telefoneText.setText(contato.getNumero());
        ImageView removeButton = viewItem.findViewById(R.id.removeContato);
        removeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Contato contato = getItem(position);
                Adapter.this.callback.onTrashClick(contato);
            }
        });
        return viewItem;
    }
}
