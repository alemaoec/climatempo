package com.diogosoares.climatempo.models;

import java.io.Serializable;
import java.util.ArrayList;

public class Contato implements Serializable {

    private long id;
    private String nome;
    private String numero;
    private String email;
    private String endereco;

    public Contato(long id, String nome, String numero, String email, String endereco) {
        this.id = id;
        this.nome = nome;
        this.numero = numero;
        this.email = email;
        this.endereco = endereco;
    }

    @Override
    public String toString() {
        return this.nome + " " + this.numero;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getNumero() {
        return numero;
    }

    public void setNumero(String numero) {
        this.numero = numero;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public long getId() {
        return id;
    }
    public void setId(long id) { this.id = id; }
}
